Important Design Details
========================

* File protection

  **cj-set-unset-files-protection.yml**
  If case of import for 'non-production' environment files from list 'cj_protect_files' should be 'read-only'. One of files is 'credentials.xml'. Why?
  
  Each time you create a new user/password Jenkins generates unique ID which may be added to job config files (or another configs). It means that you should add/edit a credential on one server only. So, the most safe way is to manage credentials on production server. If you need it on dev-env - do export from 'prod' and do import to 'dev' from 'prod'.

  As a result, if you edit credentials on dev-environment Jenkins shows exception (it should help to user understand that this is a wrong place for this change)

* Custom update site

  Custom update site provided by [Juseppe application](http://vl-tlv-scm-03:7990/projects/BUIL/repos/ansible-role-juseppe/browse). Juseppe is a web application which scans a folder with plugins and generates "update-center.json" file (list of available plugins with versions). When we supply this file to Jenkins instead of default (http://updates.jenkins-ci.org/update-center.json), we can control which plugins may be installed.

* Use Jenkins CLI with LDAP

  After LDAP is configured we need to use jenkins cli with authentication. To use the cli without user/password, first user in admins list is updated with jenkins-user public ssh key. In other words, when we execute jenkins cli like jenkins (devbuild in our case) user - no authentication is needed.

* Set JENKINS_SUFFIX

  JENKINS_SUFFIX is a global Jenkins variable describes current Jenkins environment (dev, stage, prod, ...). It reflects the Ansible group name (see [inventory file in jenkins_ci](http://vl-tlv-scm-03:7990/projects/BUIL/repos/jenkins_ci/browse/inventories/bmc_lan)). 

* Import

  Import doesn't use Jenkins API, just works with files, so Jenkins should be stopped in import beginning.

  Import copies config-files from import/export folder to the Jenkins home directory. If variable 'cj_import_to_production' is true, protected files will be available for writing.
  
  JENKINS_SUFFIX will be updated after import.
  